#![allow(non_snake_case)]
use std::io;

//need to write a result enum for errors on the program.

fn main() {
RPN(); 
}


fn RPN() -> u64 {
 enum Errors{
    DivideByZero,
    IntegerOverflow,
    StackUnderflow,
    StackNotConsumed,
    Parse
 }


loop {     
         println!("Please enter an equation in the format of Reverse Polish Notation:"); 
        let mut  user_input = String::new();
        io::stdin().read_line(&mut user_input).expect("Failed to read line"); 
        println!("{}",user_input);
       


        if user_input == String::from("exit") {
            println!("Exiting the program!");
         return 0; 
        } 
        
        let mut stack: Vec<u64> = Vec::new(); 
  

        for iter in user_input.split_whitespace(){
        println!("{:?}", stack);  
         match iter {

                "+" |"*"| "-"| "/" => {
                    let first_pop =  stack.pop().unwrap();
                               
                     let second_pop = stack.pop().unwrap();
                               
                     match iter {
                            "+" => { 
              
                                     stack.push(first_pop + second_pop);   
                  
                                     }
                            "-" => { 
              
                                     stack.push(second_pop - first_pop);   
                  
                                     }
                            "*" => { 
                                     stack.push(first_pop * second_pop);   
                  
                                     }
                            "/" => { 
              
                                     stack.push(second_pop / first_pop);   
                  
                                         }
                    
                             _ => panic!("Type not accounted for")
                         }//end of operator match
                    }//end of match for popping values

                    _ => {
                           match iter.parse::<u64>() { 
                                Ok(number) => stack.push(number),
                                
                                Err(_) => println!("Unable to parse"),
                           }
                    }
                                 

                    }//end of match

            }//end of for loop
            println!("{}", stack[0]);
            return stack[0];

         }//end of loop 
}//end of rpn function
    



